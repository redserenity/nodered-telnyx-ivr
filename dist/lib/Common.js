"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TelnyxApiHost = 'https://api.telnyx.com/';
class Guid {
}
Guid.ReplaceX = (c) => {
    const r = Math.random() * 16 | 0;
    const v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
};
Guid.NewGuid = () => 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, Guid.ReplaceX);
exports.Guid = Guid;
