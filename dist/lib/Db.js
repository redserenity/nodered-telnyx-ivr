"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sqlite3 = require("sqlite3");
class DbSingleton {
    constructor() {
        this.Close = () => DbSingleton.dbInstance.close();
        this.InitDb = () => {
            let pResolve;
            let pReject;
            const dbPromise = new Promise(((resolve, reject) => {
                pResolve = resolve;
                pReject = reject;
            }));
            DbSingleton.dbInstance = new sqlite3.Database('./telnyx.db', (error) => {
                if (error) {
                    console.log(error);
                    pReject(error);
                    return;
                }
                this.CreateTable();
                pResolve();
            });
            return dbPromise;
        };
        this.CreateTable = () => {
            const sql = `
      CREATE TABLE IF NOT EXISTS CallControl (
        CallControlId TEXT NOT NULL,
        CallLegId TEXT NOT NULL,
        CallSessionId TEXT NOT NULL,
        CurrentEvent TEXT NOT NULL,
        LastNode TEXT NOT NULL,
        DataObj TEXT NOT NULL,
        PRIMARY KEY (CallControlId, CallLegId)
      )
    `;
            DbSingleton.dbInstance.run(sql, (err => console.log(err)));
        };
        this.UpdateCall = (nodeId, data) => {
            DbSingleton.dbInstance.run('REPLACE INTO CallControl (CallControlId, CallLegId, CallSessionId, CurrentEvent, LastNode, DataObj VALUES (?,?,?,?,?,?)', [data.payload.call_control_id, data.payload.call_leg_id, data.payload.call_session_id, data.event_type, nodeId, JSON.stringify(data)], (err => console.log(err)));
        };
    }
    static get Instance() {
        if (!DbSingleton._Instance) {
            DbSingleton._Instance = new DbSingleton();
        }
        return DbSingleton._Instance;
    }
}
exports.DbSingleton = DbSingleton;
