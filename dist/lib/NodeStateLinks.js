"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NodeStateLinks {
    constructor() {
        this._Nodes = new Map();
        this.AddNode = (node) => this._Nodes.set(node.id, node);
        this.HasNode = (nodeId) => this._Nodes.has(nodeId);
        this.GetNode = (nodeId) => this._Nodes.get(nodeId);
        this.RemoveNode = (nodeId) => this._Nodes.delete(nodeId);
        this.ForwardMessage = (nodeId, msg) => {
            for (let [itemId, item] of this._Nodes) {
                if (itemId === nodeId) {
                    item.receive(msg);
                    break;
                }
            }
        };
    }
    static get Instance() {
        if (!NodeStateLinks._Instance) {
            NodeStateLinks._Instance = new NodeStateLinks();
        }
        return NodeStateLinks._Instance;
    }
}
exports.NodeStateLinks = NodeStateLinks;
