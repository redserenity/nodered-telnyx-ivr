"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseNode_1 = require("./BaseNode");
const CallControlMessage_1 = require("../models/CallControlMessage");
const StatusFill_1 = require("../models/StatusFill");
const StatusShape_1 = require("../models/StatusShape");
const Common_1 = require("./Common");
const request = require("request");
class TelnyxApi extends BaseNode_1.BaseNode {
    constructor() {
        super();
        this.OnClose = () => { this.Status({}); };
        this.Calls = (Action, Msg, Callback) => {
            const Url = this.FormatUrl(Msg.payload, 'calls/{call_control_id}/actions/' + Action, 'calls');
            this.CallApi(Url, Msg, Callback);
        };
        this.CallApi = (Url, Msg, Callback) => {
            const options = {
                auth: {
                    user: '07d05354-b7a3-4771-8265-a7eb0e924d4b',
                    pass: 'HW_JbGaRTCGS_EIlgjcH2w'
                }
            };
            const ResponseMsg = {
                _msgid: Msg._msgid,
                req: Msg.req,
                res: Msg.res
            };
            request.post(Common_1.TelnyxApiHost + Url, options, ((error, response, body) => {
                if (error) {
                    const errMsg = this.HandleError(error, Msg, Common_1.TelnyxApiHost + Url);
                    Callback(undefined, errMsg);
                    return;
                }
                ResponseMsg.statusCode = response.statusCode;
                ResponseMsg.headers = response.headers;
                ResponseMsg.responseUrl = response.request.uri.href;
                ResponseMsg.payload = body;
                try {
                    const msgBody = JSON.parse(body);
                    ResponseMsg.payload = {
                        _NEXT: CallControlMessage_1.CallControlMessageNEXT,
                        EventType: Msg.payload.EventType,
                        EventPayload: msgBody
                    };
                }
                catch (e) {
                    this.Warn('Could not convert body to JSON', Msg);
                }
                this.Status({});
                Callback(Msg);
            }));
        };
        this.HandleError = (error, origMsg, requestUrl) => {
            switch (error.code) {
                case 'ETIMEDOUT':
                case 'ESOCKETTIMEDOUT':
                    this.Error('No response from Telnyx Api', origMsg);
                    this.Status({ fill: StatusFill_1.StatusFill.Red, shape: StatusShape_1.StatusShape.Ring, text: 'No Response' });
                    break;
                default:
                    this.Error(error.msg, origMsg);
                    this.Status({ fill: StatusFill_1.StatusFill.Red, shape: StatusShape_1.StatusShape.Ring, text: error.code });
            }
            origMsg.payload.Error = {
                Message: error.toString() + ' : ' + requestUrl,
                Code: error.code
            };
            origMsg.statusCode = error.code;
            return origMsg;
        };
    }
    FormatUrl(msg, success, fallBack) {
        if (!msg.EventPayload || !msg.EventPayload.payload || !msg.EventPayload.payload.call_control_id) {
            return fallBack;
        }
        const payload = msg.EventPayload.payload;
        return success.split(/({\w+?})/g).map((v) => {
            let replaced = v.replace(/\$/g, "");
            return payload[replaced] || replaced;
        }).join('');
    }
}
exports.TelnyxApi = TelnyxApi;
