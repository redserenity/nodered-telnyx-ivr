"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NodeEvent_1 = require("../models/NodeEvent");
const Db_1 = require("./Db");
const NodeStateLinks_1 = require("./NodeStateLinks");
class BaseNode {
    constructor() {
        this.OnInput = (msg) => { };
        this.OnClose = () => { };
    }
    Setup(RED, child, config) {
        this.Node = child;
        this.Config = config;
        RED.nodes.createNode(this.Node, this.Config);
        this.Node.on(NodeEvent_1.NodeEvent.Input, this.OnInput);
        this.Node.on(NodeEvent_1.NodeEvent.Close, this.OnClose);
        NodeStateLinks_1.NodeStateLinks.Instance.AddNode(this.Node);
        this.Db = Db_1.DbSingleton.Instance;
    }
    Send(msg) {
        this.Node.send(msg);
    }
    Error(errorMsg, origMsg) {
        this.Node.error(errorMsg, origMsg);
    }
    Warn(errorMsg, origMsg) {
        this.Node.warn(errorMsg, origMsg);
    }
    Log(errorMsg, origMsg) {
        this.Node.log(errorMsg, origMsg);
    }
    Status(shape, fill, text) {
        this.Node.status({ shape: shape, fill: fill, text: text });
    }
    ClearStatus() {
        this.Node.status({});
    }
    NodeContext(Key, Value = undefined) {
        if (Value === undefined) {
            return this.Node.context().get(Key);
        }
        this.Node.context().set(Key, Value);
    }
    FlowContext(Key, Value = undefined) {
        if (Value === undefined) {
            return this.Node.context().flow.get(Key);
        }
        this.Node.context().flow.set(Key, Value);
    }
    GlobalContext(Key, Value = undefined) {
        if (Value === undefined) {
            return this.Node.context().global.get(Key);
        }
        this.Node.context().global.set(Key, Value);
    }
}
exports.BaseNode = BaseNode;
