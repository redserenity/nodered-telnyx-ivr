"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseNode_1 = require("./BaseNode");
const Common_1 = require("./Common");
const axios_1 = require("axios");
class TelnyxApiNode extends BaseNode_1.BaseNode {
    constructor() {
        super();
        this.OnClose = () => { this.ClearStatus(); };
        this.Calls = (Action, callControlId, requestBody) => {
            const Url = this.FormatUrl(callControlId, 'calls/{call_control_id}/actions/' + Action, 'calls');
            return this.CallApi(Url, requestBody);
        };
        this.CallApi = (Url, requestBody = {}) => {
            const config = {
                withCredentials: true,
                auth: {
                    username: this.ApiAuth.user,
                    password: this.ApiAuth.password
                }
            };
            requestBody.client_state = Buffer.from(this.Node.id).toString('base64');
            return axios_1.default.post(Common_1.TelnyxApiHost + Url, requestBody, config);
        };
    }
    get ApiAuth() {
        return this.FlowContext('TelnyxAuth');
    }
    FormatUrl(callControlId, success, fallBack) {
        if (!callControlId || callControlId === '') {
            return fallBack;
        }
        return success.replace('{call_control_id}', callControlId);
    }
}
exports.TelnyxApiNode = TelnyxApiNode;
