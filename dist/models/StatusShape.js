"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StatusShape;
(function (StatusShape) {
    StatusShape["Dot"] = "dot";
    StatusShape["Ring"] = "ring";
})(StatusShape = exports.StatusShape || (exports.StatusShape = {}));
