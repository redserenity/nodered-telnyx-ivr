"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TelnyxCallDirection;
(function (TelnyxCallDirection) {
    TelnyxCallDirection["Incoming"] = "incoming";
    TelnyxCallDirection["Outgoing"] = "outgoing";
})(TelnyxCallDirection = exports.TelnyxCallDirection || (exports.TelnyxCallDirection = {}));
