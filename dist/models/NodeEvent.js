"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NodeEvent;
(function (NodeEvent) {
    NodeEvent["Input"] = "input";
    NodeEvent["Close"] = "close";
})(NodeEvent = exports.NodeEvent || (exports.NodeEvent = {}));
