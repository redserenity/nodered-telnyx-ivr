"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TelnyxChannel;
(function (TelnyxChannel) {
    TelnyxChannel["Single"] = "single";
    TelnyxChannel["Dual"] = "dual";
})(TelnyxChannel = exports.TelnyxChannel || (exports.TelnyxChannel = {}));
