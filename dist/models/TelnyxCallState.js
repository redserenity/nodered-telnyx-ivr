"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TelnyxCallState;
(function (TelnyxCallState) {
    TelnyxCallState["Answered"] = "answered";
    TelnyxCallState["Bridged"] = "bridged";
    TelnyxCallState["Hangup"] = "hangup";
    TelnyxCallState["Parked"] = "parked";
})(TelnyxCallState = exports.TelnyxCallState || (exports.TelnyxCallState = {}));
