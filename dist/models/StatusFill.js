"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StatusFill;
(function (StatusFill) {
    StatusFill["Red"] = "red";
    StatusFill["Green"] = "green";
    StatusFill["Yellow"] = "yellow";
    StatusFill["Blue"] = "blue";
    StatusFill["Grey"] = "grey";
})(StatusFill = exports.StatusFill || (exports.StatusFill = {}));
