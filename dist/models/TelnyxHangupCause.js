"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TelnyxHangupCause;
(function (TelnyxHangupCause) {
    TelnyxHangupCause["CallRejected"] = "call_rejected";
    TelnyxHangupCause["NormalClearing"] = "normal_clearing";
    TelnyxHangupCause["OriginatorCancel"] = "originator_cancel";
    TelnyxHangupCause["Timeout"] = "timeout";
    TelnyxHangupCause["TimeLimit"] = "time_limit";
    TelnyxHangupCause["UserBusy"] = "user_busy";
    TelnyxHangupCause["NotFound"] = "not_found";
})(TelnyxHangupCause = exports.TelnyxHangupCause || (exports.TelnyxHangupCause = {}));
