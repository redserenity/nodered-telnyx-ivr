"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TelnyxCallStatus;
(function (TelnyxCallStatus) {
    TelnyxCallStatus["Valid"] = "valid";
    TelnyxCallStatus["Invalid"] = "invalid";
    TelnyxCallStatus["CallHangup"] = "call_hangup";
    TelnyxCallStatus["Completed"] = "completed";
})(TelnyxCallStatus = exports.TelnyxCallStatus || (exports.TelnyxCallStatus = {}));
