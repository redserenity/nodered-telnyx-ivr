"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TelnyxCallEvents;
(function (TelnyxCallEvents) {
    TelnyxCallEvents["CallInitiated"] = "call_initiated";
    TelnyxCallEvents["Answered"] = "call_answered";
    TelnyxCallEvents["Bridged"] = "call_bridged";
    TelnyxCallEvents["Hangup"] = "call_hangup";
    TelnyxCallEvents["DTMF"] = "dtmf";
    TelnyxCallEvents["GatherEnded"] = "gather_ended";
    TelnyxCallEvents["PlaybackStarted"] = "playback_started";
    TelnyxCallEvents["RecordingSaved"] = "recording_saved";
    TelnyxCallEvents["SpeakStarted"] = "speak_started";
    TelnyxCallEvents["SpeakEnded"] = "speak_ended";
})(TelnyxCallEvents = exports.TelnyxCallEvents || (exports.TelnyxCallEvents = {}));
