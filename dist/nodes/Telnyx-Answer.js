"use strict";
const NodeRed_1 = require("../lib/NodeRed");
const TelnyxApiNode_1 = require("../lib/TelnyxApiNode");
module.exports = (RED) => {
    'use strict';
    class TelnyxAnswer extends TelnyxApiNode_1.TelnyxApiNode {
        constructor(config) {
            super();
            this.OnInput = (msg) => {
                if (msg.payload.event_type === NodeRed_1.TelnyxCallEvents.CallInitiated) {
                    this.Db.UpdateCall(this.Node.id, msg.payload);
                    this.Calls(NodeRed_1.TelnyxCallsAction.Answer, msg.payload.payload.call_control_id)
                        .catch((error) => this.Send([undefined, error]));
                    return;
                }
                if (msg.payload.event_type === NodeRed_1.TelnyxCallEvents.Answered) {
                    this.Db.UpdateCall(this.Node.id, msg.payload);
                    this.Send([msg, undefined]);
                }
            };
            this.Setup(RED, this, config);
        }
    }
    RED.nodes.registerType('telnyx-answer', TelnyxAnswer);
};
