"use strict";
const NodeRed_1 = require("../lib/NodeRed");
const TelnyxApiNode_1 = require("../lib/TelnyxApiNode");
module.exports = (RED) => {
    'use strict';
    class TelnyxHangup extends TelnyxApiNode_1.TelnyxApiNode {
        constructor(config) {
            super();
            this.OnInput = (msg) => {
                this.Calls(NodeRed_1.TelnyxCallsAction.Hangup, msg.payload.payload.call_control_id);
            };
            this.Setup(RED, this, config);
        }
    }
    RED.nodes.registerType('telnyx-hangup', TelnyxHangup);
};
