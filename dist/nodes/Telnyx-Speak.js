"use strict";
const NodeRed_1 = require("../lib/NodeRed");
const TelnyxApiNode_1 = require("../lib/TelnyxApiNode");
module.exports = (RED) => {
    'use strict';
    class TelnyxSpeak extends TelnyxApiNode_1.TelnyxApiNode {
        constructor(config) {
            super();
            this.OnInput = (msg) => {
                if (msg.payload.event_type === NodeRed_1.TelnyxCallEvents.SpeakStarted) {
                    this.Status(NodeRed_1.StatusShape.Ring, NodeRed_1.StatusFill.Blue, 'Speak started...');
                    return;
                }
                if (msg.payload.event_type === NodeRed_1.TelnyxCallEvents.SpeakEnded) {
                    this.Status(NodeRed_1.StatusShape.Dot, NodeRed_1.StatusFill.Blue, 'Finished speaking.');
                    this.Send(msg);
                    return;
                }
                const speakRequest = {
                    payload: this.Config.speak,
                    payload_type: this.Config.speakType,
                    service_level: this.Config.serviceLevel,
                    voice: this.Config.gender,
                    language: this.Config.language
                };
                this.Calls(NodeRed_1.TelnyxCallsAction.Speak, msg.payload.payload.call_control_id, speakRequest)
                    .catch((error) => this.Status(NodeRed_1.StatusShape.Dot, NodeRed_1.StatusFill.Red, error.message));
            };
            this.Setup(RED, this, config);
        }
    }
    RED.nodes.registerType('telnyx-speak', TelnyxSpeak);
};
