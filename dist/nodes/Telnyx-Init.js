"use strict";
const BaseNode_1 = require("../lib/BaseNode");
const NodeRed_1 = require("../lib/NodeRed");
module.exports = (RED) => {
    'use strict';
    class TelnyxInit extends BaseNode_1.BaseNode {
        constructor(config) {
            super();
            this.OnClose = () => this.Db.Close();
            this.Setup(RED, this, config);
            this.Db.InitDb()
                .then((result) => this.Status(NodeRed_1.StatusShape.Dot, NodeRed_1.StatusFill.Green, 'Ready'))
                .catch((error) => this.Status(NodeRed_1.StatusShape.Ring, NodeRed_1.StatusFill.Red, error.message));
            this.FlowContext('TelnyxAuth', this.Node.credentials);
        }
    }
    RED.nodes.registerType('telnyx-init', TelnyxInit, {
        credentials: {
            user: { type: 'text' },
            password: { type: 'password' }
        }
    });
};
