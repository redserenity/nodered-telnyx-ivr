"use strict";
const TelnyxApiNode_1 = require("../lib/TelnyxApiNode");
module.exports = (RED) => {
    'use strict';
    class TelnyxCallQueue extends TelnyxApiNode_1.TelnyxApiNode {
        constructor(config) {
            super();
            this.OnInput = (msg) => {
            };
            this.Setup(RED, this, config);
        }
    }
    RED.nodes.registerType('telnyx-callqueue', TelnyxCallQueue);
};
