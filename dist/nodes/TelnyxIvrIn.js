"use strict";
const Common_1 = require("../lib/Common");
const BaseNode_1 = require("../lib/BaseNode");
module.exports = (RED) => {
    'use strict';
    class TelnyxIvrIn extends BaseNode_1.BaseNode {
        constructor(config) {
            super();
            this.RegisterRoute = () => RED.httpNode.post(this.GuidUrl, this.SendBody);
            this.SendBody = (req, res) => {
                const msgId = RED.util.generateId();
                res._msgid = msgId;
                this.Send({
                    _msgid: msgId,
                    req: req,
                    res: { _res: res },
                    payload: req.body
                });
            };
            this.OnClose = () => {
                RED.httpNode._router.stack.forEach((Route, Index, Routes) => {
                    if (!Route.route)
                        return;
                    if (Route.route.path === this.GuidUrl && Route.route.methods['post']) {
                        console.log('Removing route ' + Route.route.path);
                        Routes.splice(Index, 1);
                    }
                });
            };
            this.Setup(RED, this, config);
            this.RegisterRoute();
        }
        get GuidUrl() { return '/ivr/' + this.Config.url; }
    }
    RED.nodes.registerType('telnyx-ivr-in', TelnyxIvrIn, {
        settings: {
            telnyxIvrIn_urlGuid: {
                value: Common_1.Guid.NewGuid(),
                exportable: true
            }
        }
    });
};
