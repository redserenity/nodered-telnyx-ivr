"use strict";
const BaseNode_1 = require("../lib/BaseNode");
const Common_1 = require("../lib/Common");
const NodeRed_1 = require("../lib/NodeRed");
const NodeStateLinks_1 = require("../lib/NodeStateLinks");
module.exports = (RED) => {
    'use strict';
    class TelnyxIvrIn extends BaseNode_1.BaseNode {
        constructor(config) {
            super();
            this.RegisterRoute = () => RED.httpNode.post(this.GuidUrl, this.HandleRequest);
            this.HandleRequest = (req, res) => {
                this.Status(NodeRed_1.StatusShape.Ring, NodeRed_1.StatusFill.Yellow, 'Receiving call...');
                if (!req.body) {
                    throw new Error('No body in request');
                }
                const msgId = RED.util.generateId();
                res._msgid = msgId;
                this.ValidatePayload(req.body);
                if (req.body.payload.client_state) {
                    NodeStateLinks_1.NodeStateLinks.Instance.ForwardMessage(req.body.payload.client_state, {
                        _msgid: msgId,
                        req: req,
                        res: res,
                        payload: req.body
                    });
                    return;
                }
                this.Status(NodeRed_1.StatusShape.Ring, NodeRed_1.StatusFill.Yellow, 'Receiving call from ' + req.body.payload.from);
                this.Send({
                    _msgid: msgId,
                    req: req,
                    res: res,
                    payload: req.body
                });
                this.Status(NodeRed_1.StatusShape.Dot, NodeRed_1.StatusFill.Blue, 'Received call from ' + req.body.payload.from);
            };
            this.OnClose = () => {
                RED.httpNode._router.stack.forEach((Route, Index, Routes) => {
                    if (!Route.route)
                        return;
                    if (Route.route.path === this.GuidUrl && Route.route.methods['post']) {
                        console.log('Removing route ' + Route.route.path);
                        Routes.splice(Index, 1);
                    }
                });
            };
            this.ValidatePayload = (data) => {
                if (!data.payload) {
                    throw new Error('Missing payload from request body.');
                }
                if (data.payload.client_state) {
                    const stateBuffer = Buffer.from(data.payload.client_state, 'base64').toString('ascii');
                    data.payload.client_state = stateBuffer;
                }
            };
            this.Setup(RED, this, config);
            this.RegisterRoute();
        }
        get GuidUrl() { return '/ivr/' + this.Config.url; }
    }
    RED.nodes.registerType('telnyx-ivr-in', TelnyxIvrIn, {
        settings: {
            telnyxIvrIn_urlGuid: {
                value: Common_1.Guid.NewGuid(),
                exportable: true
            }
        }
    });
};
