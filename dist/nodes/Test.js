"use strict";
const BaseNode_1 = require("../lib/BaseNode");
module.exports = (RED) => {
    class Test extends BaseNode_1.BaseNode {
        constructor(props) {
            super();
            this.Setup(RED, this, props);
        }
    }
    RED.nodes.registerType('test', Test);
};
