import sqlite3 = require('sqlite3');
import { Database } from 'sqlite3';
import { TelnyxEventMsg } from '../models/TelnyxEventMsg';

export class DbSingleton {

  static get Instance(): DbSingleton {
    if (!DbSingleton._Instance) {
      DbSingleton._Instance = new DbSingleton();
    }
    return DbSingleton._Instance;
  }

  private static _Instance: DbSingleton;

  private static dbInstance: Database;

  private constructor() { }

  Close = () => DbSingleton.dbInstance.close();

  InitDb = (): Promise<any> => {
    let pResolve: { (value?: any): void; };
    let pReject: { (reason?: any): void; };

    const dbPromise = new Promise(((resolve, reject) => {
      pResolve = resolve;
      pReject = reject;
    }));

    DbSingleton.dbInstance = new sqlite3.Database('./telnyx.db', (error) => {
      if (error) {
        console.log(error);
        pReject(error);
        return;
      }

      this.CreateTable();
      pResolve();
    });

    return dbPromise;
  };

  private CreateTable = () => {
    const sql = `
      CREATE TABLE IF NOT EXISTS CallControl (
        CallControlId TEXT NOT NULL,
        CallLegId TEXT NOT NULL,
        CallSessionId TEXT NOT NULL,
        CurrentEvent TEXT NOT NULL,
        LastNode TEXT NOT NULL,
        DataObj TEXT NOT NULL,
        PRIMARY KEY (CallControlId, CallLegId)
      )
    `;

    // TODO: Handle errors better
    DbSingleton.dbInstance.run(sql, (err => console.log(err)));
  };

  UpdateCall = (nodeId: string, data: TelnyxEventMsg) => {
    DbSingleton.dbInstance.run(
      'REPLACE INTO CallControl (CallControlId, CallLegId, CallSessionId, CurrentEvent, LastNode, DataObj VALUES (?,?,?,?,?,?)',
      [data.payload.call_control_id, data.payload.call_leg_id, data.payload.call_session_id, data.event_type, nodeId, JSON.stringify(data)],
      (err => console.log(err)));
  };

}


/*

run(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.run(sql, params, function (err) {
        if (err) {
          console.log('Error running sql ' + sql)
          console.log(err)
          reject(err)
        } else {
          resolve({ id: this.lastID })
        }
      })
    })


 */
