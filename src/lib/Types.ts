import { IMessage } from '../models/IMessage';
import { INodeProperties } from '../models/INodeProperties';
import { IRed } from '../models/IRed';
import { TelnyxEventMsg } from '../models/TelnyxEventMsg';

export type NodeRegistration = (RED: IRed) => void
export type NodeInitializer = new(config: INodeProperties) => void
export type MessageCallback = (msg: IMessage<any>) => void
export type TelnyxResponse = (success?: IMessage<TelnyxEventMsg>, error?: IMessage<TelnyxEventMsg>) => void;


