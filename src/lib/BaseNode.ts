import { IMessage } from '../models/IMessage';
import { INodeProperties } from '../models/INodeProperties';
import { IRed } from '../models/IRed';
import { IStatusUpdate } from '../models/IStatusUpdate';
import { IThisNode } from '../models/IThisNode';
import { NodeEvent } from '../models/NodeEvent';
import { StatusFill } from '../models/StatusFill';
import { StatusShape } from '../models/StatusShape';
import { TelnyxEventMsg } from '../models/TelnyxEventMsg';
import { DbSingleton } from './Db';
import { NodeStateLinks } from './NodeStateLinks';

export class BaseNode {

	protected Node!: IThisNode;
	public Config!: INodeProperties;
	protected Db!: DbSingleton;

	Setup(RED: IRed, child: any, config: INodeProperties) {
		this.Node   = <IThisNode> child;
		this.Config = config;
		RED.nodes.createNode(this.Node, this.Config);

		this.Node.on(NodeEvent.Input, this.OnInput);
		this.Node.on(NodeEvent.Close, this.OnClose);

		NodeStateLinks.Instance.AddNode(this.Node);

		this.Db = DbSingleton.Instance;
	}

	protected OnInput = (msg: IMessage<TelnyxEventMsg>) => {};
	protected OnClose = () => {};

	protected Send(msg: IMessage<any> | Array<IMessage<any> | undefined> | undefined): void {
		this.Node.send(msg);
	}

	protected Error(errorMsg: string, origMsg: IMessage<any>): void {
		this.Node.error(errorMsg, origMsg);
	}

	protected Warn(errorMsg: string, origMsg: IMessage<any>): void {
		this.Node.warn(errorMsg, origMsg);
	}

	protected Log(errorMsg: string, origMsg: IMessage<any>): void {
		this.Node.log(errorMsg, origMsg);
	}

	protected Status(shape: StatusShape, fill: StatusFill, text: string): void {
		this.Node.status(<IStatusUpdate> { shape: shape, fill: fill, text: text });
	}

	protected ClearStatus(): void {
		this.Node.status(<IStatusUpdate> {});
	}


	NodeContext(Key: string, Value: any = undefined): any {
		if (Value === undefined) {
			return this.Node.context().get(Key);
		}

		this.Node.context().set(Key, Value);
	}

	FlowContext(Key: string, Value: any = undefined): any {
		if (Value === undefined) {
			return this.Node.context().flow.get(Key);
		}

		this.Node.context().flow.set(Key, Value);
	}

	GlobalContext(Key: string, Value: any = undefined): any {
		if (Value === undefined) {
			return this.Node.context().global.get(Key);
		}

		this.Node.context().global.set(Key, Value);
	}
}
