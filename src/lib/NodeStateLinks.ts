import { IMessage } from '../models/IMessage';
import { IThisNode } from '../models/IThisNode';
import { TelnyxEventMsg } from '../models/TelnyxEventMsg';

export class NodeStateLinks {

  private _Nodes: Map<string, IThisNode> = new Map<string, IThisNode>();

  static get Instance(): NodeStateLinks {
    if (!NodeStateLinks._Instance) {
      NodeStateLinks._Instance = new NodeStateLinks();
    }
    return NodeStateLinks._Instance;
  }

  private static _Instance: NodeStateLinks;

  private constructor() { }

  public AddNode = (node: IThisNode): Map<string, IThisNode> => this._Nodes.set(node.id, node);
  public HasNode = (nodeId: string): boolean => this._Nodes.has(nodeId);
  public GetNode = (nodeId: string): IThisNode | undefined => this._Nodes.get(nodeId);
  public RemoveNode = (nodeId: string): boolean => this._Nodes.delete(nodeId);

  public ForwardMessage = (nodeId: string, msg: IMessage<TelnyxEventMsg>) => {
    for (let [itemId, item] of this._Nodes) { if (itemId === nodeId) { item.receive(msg); break; } }
  };

}
