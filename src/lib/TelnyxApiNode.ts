import { ICredentials } from '../models/ICredentials';
import { IMessage } from '../models/IMessage';
import { IStatusUpdate } from '../models/IStatusUpdate';
import { StatusFill } from '../models/StatusFill';
import { StatusShape } from '../models/StatusShape';
import { TelnyxCallsAction } from '../models/TelnyxCallsAction';
import { TelnyxEventMsg } from '../models/TelnyxEventMsg';
import { TelnyxRequest } from '../models/TelnyxRequest';
import { BaseNode } from './BaseNode';
import { TelnyxApiHost } from './Common';
import { TelnyxResponse } from './Types';
import axios, {
	AxiosRequestConfig,
	AxiosResponse,
	AxiosError,
	AxiosInstance,
	AxiosAdapter,
	Cancel,
	CancelToken,
	CancelTokenSource,
	Canceler
} from 'axios';

export abstract class TelnyxApiNode extends BaseNode {

	get ApiAuth(): ICredentials {
		return this.FlowContext('TelnyxAuth');
	}

	protected constructor() {
		super();
	}

	OnClose = () => { this.ClearStatus(); };

	Calls = (Action: TelnyxCallsAction, callControlId: string, requestBody?: TelnyxRequest): Promise<TelnyxResponse> => {
		const Url = this.FormatUrl(callControlId, 'calls/{call_control_id}/actions/' + Action, 'calls');
		return this.CallApi(Url, requestBody);
	};

	CallApi = (Url: string, requestBody: TelnyxRequest = {}): Promise<TelnyxResponse> => {
		const config: AxiosRequestConfig = {
			withCredentials: true,
			auth: {
				username: this.ApiAuth.user,
				password: this.ApiAuth.password
			}
		};

		requestBody.client_state = Buffer.from(this.Node.id).toString('base64');

		return axios.post(TelnyxApiHost + Url, requestBody, config);

		/*let pResolve: { (value?: any): void; };
		let pReject: { (reason?: any): void; };

		const promise = new Promise<TelnyxResponse>(((resolve, reject) => {
			pResolve = resolve;
			pReject = reject;
		}));

		request.post(TelnyxApiHost + Url, options, ((error, response, body) => {
			if (error) {
				pReject(error);
				this.HandleError(error, Msg, TelnyxApiHost + Url);
				return;
			}

			try {
				const msgBody = <TelnyxResponse> JSON.parse(body);
				pResolve(msgBody);
				// TODO: Why are we hardcoding this in an abstract class?
				this.Status({ fill: StatusFill.Green, shape: StatusShape.Dot, text: 'Answered call from ' + Msg.payload.payload.from });
			}	catch (e) { pReject(e); }

		}));

		return promise;*/
	};

	/*HandleError = (error: any, origMsg: IMessage<TelnyxEventMsg>, requestUrl: string) => {
		console.log(error);
		switch (error.code) {
			case 'ETIMEDOUT':
			case 'ESOCKETTIMEDOUT':
				this.Error('No response from Telnyx Api', origMsg);
				this.Status({ fill: StatusFill.Red, shape: StatusShape.Ring, text: 'No Response from API' });
				break;
			default:
				this.Error(error.message, origMsg);
				this.Status({ fill: StatusFill.Red, shape: StatusShape.Ring, text: error.message });
		}
	};*/

	private FormatUrl(callControlId: string, success: string, fallBack: string): string {
		if (!callControlId || callControlId === '') {
			return fallBack;
		}

		return success.replace('{call_control_id}', callControlId);

/*		const payload: { [index: string]: any } = msg.payload;

		return success.split(/({\w+?})/g).map((v) => {
			let replaced = v.replace(/\$/g,"");
			return payload[replaced] || replaced;
		}).join('');*/
	}
}
