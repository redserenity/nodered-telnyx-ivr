import { IMessage, INodeProperties, IRed, TelnyxCallsAction, TelnyxEventMsg } from '../lib/NodeRed';
import { TelnyxApiNode } from '../lib/TelnyxApiNode';


export = (RED: IRed) => {
	'use strict';

	class TelnyxBlacklist extends TelnyxApiNode {

		constructor(config: INodeProperties) {
			super();
			this.Setup(RED, this, config);
		}

		protected OnInput = (msg: IMessage<TelnyxEventMsg>) => {
			this.Send(msg);
		};

	}

	RED.nodes.registerType('telnyx-blacklist', TelnyxBlacklist);
};
