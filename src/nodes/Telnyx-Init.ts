import { BaseNode } from '../lib/BaseNode';
import { DbSingleton } from '../lib/Db';
import { INodeProperties, IRed, StatusFill, StatusShape } from '../lib/NodeRed';

export = (RED: IRed) => {
  'use strict';

  class TelnyxInit extends BaseNode {

    constructor(config: INodeProperties) {
      super();
      this.Setup(RED, this, config);

      this.Db.InitDb()
        .then((result) => this.Status(StatusShape.Dot, StatusFill.Green, 'Ready'))
        .catch((error: Error) => this.Status(StatusShape.Ring, StatusFill.Red, error.message));

      this.FlowContext('TelnyxAuth', this.Node.credentials);
    }

    OnClose = (): void => this.Db.Close();

  }

  RED.nodes.registerType('telnyx-init', TelnyxInit, {
    credentials: {
      user: { type: 'text' },
      password: { type: 'password' }
    }
  });
};
