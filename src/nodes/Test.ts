import { IRed } from '../models/IRed';
import { INodeProperties } from '../models/INodeProperties';
import { BaseNode } from '../lib/BaseNode';

export = (RED: IRed) => {

	class Test extends BaseNode {

		constructor(props: INodeProperties) {
			super();
			this.Setup(RED, this, props);
		}

	}

	RED.nodes.registerType('test',  Test);
};
