import { IMessage, INodeProperties, IRed, TelnyxCallsAction, TelnyxEventMsg } from '../lib/NodeRed';
import { TelnyxApiNode } from '../lib/TelnyxApiNode';


export = (RED: IRed) => {
	'use strict';

	class TelnyxRouter extends TelnyxApiNode {

		constructor(config: INodeProperties) {
			super();
			this.Setup(RED, this, config);
		}

		protected OnInput = (msg: IMessage<TelnyxEventMsg>) => {

		};

	}

	RED.nodes.registerType('telnyx-router', TelnyxRouter);
};
