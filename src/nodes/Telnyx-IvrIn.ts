import { BaseNode } from '../lib/BaseNode';
import { Guid } from '../lib/Common';
import {
	INodeProperties,
	IRed,
	StatusFill,
	StatusShape,
	TelnyxEventMsg
} from '../lib/NodeRed';
import { NodeStateLinks } from '../lib/NodeStateLinks';

export = (RED: IRed) => {
	'use strict';

	class TelnyxIvrIn extends BaseNode {

		get GuidUrl() { return '/ivr/' + this.Config.url; }

		constructor(config: INodeProperties) {
			super();
			this.Setup(RED, this, config);
			this.RegisterRoute();
		}

		RegisterRoute = () => RED.httpNode.post(this.GuidUrl, this.HandleRequest);

		HandleRequest = (req: any, res: any): void => {
			this.Status(StatusShape.Ring, StatusFill.Yellow, 'Receiving call...');

			// TODO: Is throwing an Error the right way to handle this?
			if (!req.body) {
				throw new Error('No body in request');
			}

			const msgId = RED.util.generateId();
			res._msgid = msgId;

			this.ValidatePayload(req.body);

			// If we are continuing state, jump to where we left off.
			if (req.body.payload.client_state) {
				NodeStateLinks.Instance.ForwardMessage(req.body.payload.client_state, {
					_msgid: msgId,
					req: req,
					res: res,
					payload: req.body
				});
				return;
			}

			//this.Db.UpdateCall(this.Node.id, payload);

			this.Status(StatusShape.Ring, StatusFill.Yellow, 'Receiving call from ' + req.body.payload.from);

			this.Send({
				_msgid: msgId,
				req: req,
				res: res,
				payload: req.body
			});

			this.Status(StatusShape.Dot, StatusFill.Blue, 'Received call from ' + req.body.payload.from);
		};

		OnClose = (): void => {
			// @ts-ignore
			RED.httpNode._router.stack.forEach((Route: any, Index: number, Routes: Array<any>) => {
				if (!Route.route) return;

				if (Route.route.path === this.GuidUrl && Route.route.methods['post']) {
					console.log('Removing route ' + Route.route.path);
					Routes.splice(Index, 1);
				}
			});
		};

		ValidatePayload = (data: TelnyxEventMsg) => {
			// TODO: Validate entire payload
			if (!data.payload) {
				throw new Error('Missing payload from request body.');
			}

			if (data.payload.client_state) {
				const stateBuffer = Buffer.from(data.payload.client_state, 'base64').toString('ascii');
				data.payload.client_state = stateBuffer;
			}
		};

	}

	RED.nodes.registerType('telnyx-ivr-in', TelnyxIvrIn, {
		settings: {
			telnyxIvrIn_urlGuid: {
				value: Guid.NewGuid(),
				exportable: true
			}
		}
	});
};
