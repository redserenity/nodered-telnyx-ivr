import { TelnyxCallEvents, IMessage, INodeProperties, IRed, TelnyxCallsAction, TelnyxEventMsg } from '../lib/NodeRed';
import { TelnyxApiNode } from '../lib/TelnyxApiNode';


export = (RED: IRed) => {
	'use strict';

	class TelnyxAnswer extends TelnyxApiNode {

		constructor(config: INodeProperties) {
			super();
			this.Setup(RED, this, config);
		}

		protected OnInput = (msg: IMessage<TelnyxEventMsg>) => {
			if (msg.payload.event_type === TelnyxCallEvents.CallInitiated) {
				this.Db.UpdateCall(this.Node.id, msg.payload);

				this.Calls(TelnyxCallsAction.Answer, msg.payload.payload.call_control_id)
					.catch((error: any) => this.Send([ undefined, error ]));

				return;
			}

			if (msg.payload.event_type === TelnyxCallEvents.Answered) {
				this.Db.UpdateCall(this.Node.id, msg.payload);
				this.Send([ msg, undefined ]);
			}
		};

	}

	RED.nodes.registerType('telnyx-answer', TelnyxAnswer);
};
