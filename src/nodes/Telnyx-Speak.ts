import {
	IMessage,
	INodeProperties,
	IRed,
	StatusFill,
	StatusShape,
	TelnyxCallEvents,
	TelnyxCallsAction,
	TelnyxEventMsg
} from '../lib/NodeRed';
import { TelnyxApiNode } from '../lib/TelnyxApiNode';

export = (RED: IRed) => {
	'use strict';

	class TelnyxSpeak extends TelnyxApiNode {

		constructor(config: INodeProperties) {
			super();
			this.Setup(RED, this, config);
		}

		protected OnInput = (msg: IMessage<TelnyxEventMsg>) => {
			if (msg.payload.event_type === TelnyxCallEvents.SpeakStarted) {
				this.Status(StatusShape.Ring, StatusFill.Blue, 'Speak started...');
				return;
			}

			if (msg.payload.event_type === TelnyxCallEvents.SpeakEnded) {
				this.Status(StatusShape.Dot, StatusFill.Blue, 'Finished speaking.');
				this.Send(msg);
				return;
			}

			const speakRequest = {
				payload: this.Config.speak,
				payload_type: this.Config.speakType,
				service_level: this.Config.serviceLevel,
				voice: this.Config.gender,
				language: this.Config.language
			};

			this.Calls(TelnyxCallsAction.Speak, msg.payload.payload.call_control_id, speakRequest)
				.catch((error) => this.Status(StatusShape.Dot, StatusFill.Red, error.message));
		};

	}

	RED.nodes.registerType('telnyx-speak', TelnyxSpeak);
};
