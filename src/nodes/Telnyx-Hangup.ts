import { IMessage, INodeProperties, IRed, TelnyxCallsAction, TelnyxEventMsg } from '../lib/NodeRed';
import { TelnyxApiNode } from '../lib/TelnyxApiNode';


export = (RED: IRed) => {
	'use strict';

	class TelnyxHangup extends TelnyxApiNode {

		constructor(config: INodeProperties) {
			super();
			this.Setup(RED, this, config);
		}

		protected OnInput = (msg: IMessage<TelnyxEventMsg>) => {
		  this.Calls(TelnyxCallsAction.Hangup, msg.payload.payload.call_control_id);
		};

	}

	RED.nodes.registerType('telnyx-hangup', TelnyxHangup);
};
