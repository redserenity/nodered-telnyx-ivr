export interface IAuth { }
export interface IComms { }
export interface ILibrary { }
export interface ILog { }
export interface IRuntimeSettings { }
export interface IUtil { }
export interface IFlowConfig {}
export interface ISessions {}
export interface IRuntimeCredentials {}
