import {TelnyxCallEvents} from './TelnyxCallEvents';
import { TelnyxEventMsg } from './TelnyxEventMsg';

export const CallControlMessageNEXT = 'CallControlMessage_NEXT';

export interface CallControlMessage {
  _NEXT: string;
  EventType: TelnyxCallEvents;
  EventPayload: TelnyxEventMsg;
  Error?: {
    Code: number;
    Message: string;
  }
}
