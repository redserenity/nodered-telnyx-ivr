import { IRedNodes } from './IRedNodes';
import { ISettings } from './ISettings';
import express = require('express');

export interface IRed {
	nodes: IRedNodes;
	log: any;
	comms: any;
	settings: ISettings;
	events: any;
	util: any;
	httpAdmin: express.Router;
	auth: any;
	library: any;
	httpNode: express.Router;
	server: any;
	/** Returns the version of the running Node-RED environment. */
	version(): string;
	_: any;
}
