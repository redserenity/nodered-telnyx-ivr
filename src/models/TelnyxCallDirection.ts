export enum TelnyxCallDirection {
  Incoming = 'incoming',
  Outgoing = 'outgoing'
}
