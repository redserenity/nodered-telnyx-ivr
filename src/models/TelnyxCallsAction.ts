export enum TelnyxCallsAction {
  Answer = 'answer',
  Bridge = 'bridge',
  ForkStart = 'fork_start',
  ForkStop = 'fork_stop',
  GatherUsingAudio = 'gather_using_audio',
  GatherUsingSpeak = 'gather_using_speak',
  Hangup = 'hangup',
  PlayAudio = 'playback_start',
  StopAudio = 'playback_stop',
  StartRecord = 'record_start',
  StopRecord = 'record_stop',
  Reject = 'reject',
  SendDTMF = 'send_dtmf',
  Speak = 'speak',
  Transfer = 'transfer',
  Status = 'status'
}
