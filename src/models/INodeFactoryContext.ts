import { IGlobalContext } from './IGlobalContext';

export interface INodeFactoryContext {
	global: IGlobalContext
}
