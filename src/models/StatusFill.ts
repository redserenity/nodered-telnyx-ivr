export enum StatusFill {
	Red = 'red',
	Green = 'green',
	Yellow = 'yellow',
	Blue = 'blue',
	Grey = 'grey'
}
