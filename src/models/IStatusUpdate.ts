import { StatusShape } from './StatusShape';

export interface IStatusUpdate {
	fill: String
	shape: StatusShape
	text: String
}
