export interface INodes {
	addNode(node: any): void;
	loadFlows(): void;
	stopFlows(): void;
	startFlows(): void;
}
