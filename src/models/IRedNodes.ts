import { IThisNode } from './IThisNode';
import { NodeInitializer } from '../lib/Types';
import { INodeProperties } from './INodeProperties';

export interface IRedNodes {
	createNode(node: IThisNode, config: INodeProperties): void
	registerType(name: String, nodeInit: NodeInitializer, opts?: any): void
}
