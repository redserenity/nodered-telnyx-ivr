export interface IGlobalContext {
	get(key: String): any
	set(key: String, value: any): void
}
