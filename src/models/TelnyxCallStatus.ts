export enum TelnyxCallStatus {
  Valid = 'valid',
  Invalid = 'invalid',
  CallHangup = 'call_hangup',
  Completed = 'completed'
}
