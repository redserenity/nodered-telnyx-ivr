import { TelnyxCallDirection } from './TelnyxCallDirection';
import { TelnyxCallEvents } from './TelnyxCallEvents';
import { TelnyxCallState } from './TelnyxCallState';
import { TelnyxCallStatus } from './TelnyxCallStatus';
import { TelnyxChannel } from './TelnyxChannel';
import { TelnyxEventType } from './TelnyxEventType';
import { TelnyxHangupCause } from './TelnyxHangupCause';

export interface TelnyxEventMsg {
	record_type: TelnyxEventType;
	id: string;
	event_type: TelnyxCallEvents;
	created_at: Date;
	payload: {
		to: string;
		start_time: Date;
		occurred_at: Date;
		from: string;
		call_control_id: string;
		connection_id: string;
		call_leg_id: string;
		call_session_id: string;
		client_state: string;
		state: TelnyxCallState;
		hangup_cause?: TelnyxHangupCause;
		direction?: TelnyxCallDirection;
		digit?: string;
		status?: TelnyxCallStatus;
		media_url?: string;
		overlay?: boolean;
		recording_started_at?: Date;
		recording_ended_at?: Date;
		channels?: TelnyxChannel;
		recording_urls?: { [ key: string ]: string },
		public_recording_urls?: { [ key: string ]: string },
	}

}

