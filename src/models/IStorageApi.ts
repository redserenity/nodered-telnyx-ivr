import { ICredentials } from './ICredentials';
import { IRuntimeFlowConfig } from './IRuntimeFlowConfig';
import { ISettings } from './ISettings';
import { IFlowConfig, IRuntimeCredentials, ISessions } from './InterfacePlaceHolders';

export interface IStorageApi {
	init(settings: ISettings): Promise<any>;

	getFlows(): Promise<IRuntimeFlowConfig>;

	saveFlows(flows: IFlowConfig): Promise<any>;

	getCredentials(): Promise<IRuntimeCredentials>;

	saveCredentials(credentials: ICredentials): Promise<any>;

	getSettings(): Promise<ISettings>;

	saveSettings(settings: ISettings): Promise<any>;

	getSessions(): Promise<ISessions>;

	saveSessions(sessions: ISessions): Promise<any>;

	/**
	 * the type of library entry, eg flows, functions, templates
	 * @param  {[type]} type - the type of library entry, eg flows, functions, templates
	 * @param  {[type]} name - the pathname of the entry to return
	 * @return {[type]}      - Returns a promise that resolves with the result
	 */
	getLibraryEntry(type: any, name: any): any

	saveLibraryEntry(type: any, name: any, meta: any, body: any): void
}
