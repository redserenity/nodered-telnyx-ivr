export enum NodeEvent {
	Input = 'input',
	Close = 'close'
}
