import * as express from 'express';
import * as http from 'http';

import { ISettings } from './ISettings';
import { IAuth, IComms, ILibrary, ILog, IRuntimeSettings, IUtil } from './InterfacePlaceHolders';
import { INodes } from './INodes';

/**
 * Node-RED node creation api.
 */
export interface IRuntime {
	httpAdmin: express.Router;
	httpNode: express.Router;
	server: http.Server;
	auth: IAuth;
	comms: IComms;
	library: ILibrary;
	log: ILog;
	nodes: INodes;
	settings: IRuntimeSettings;
	util: IUtil;

	init(server: http.Server, settings: ISettings): void;
	start(): Promise<any>;
	stop(): void;
	version(): void;
}
