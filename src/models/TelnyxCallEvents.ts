export enum TelnyxCallEvents {
  CallInitiated = 'call_initiated',
  Answered = 'call_answered',
  Bridged = 'call_bridged',
  Hangup = 'call_hangup',
  DTMF = 'dtmf',
  GatherEnded = 'gather_ended',
  PlaybackStarted = 'playback_started',
  RecordingSaved = 'recording_saved',
  SpeakStarted = 'speak_started',
  SpeakEnded = 'speak_ended'
}
