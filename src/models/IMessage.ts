export interface IMessage<T> {
	topic?: string;
	_msgid: string;
	req?: Request;
	res: Response;
	payload: T;
	[key: string]: any;
}
