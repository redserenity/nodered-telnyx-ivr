export interface IRuntimeFlowConfig {
	getCurrentFlow(): any
	saveCurrentFlow(data: any): void
}
