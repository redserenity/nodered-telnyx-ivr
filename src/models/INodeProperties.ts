export interface INodeProperties {
	id: string;
	type: string;
	name: string;
	[key: string]: any;
}
