export enum TelnyxHangupCause {
  CallRejected = 'call_rejected',
  NormalClearing = 'normal_clearing',
  OriginatorCancel = 'originator_cancel',
  Timeout = 'timeout',
  TimeLimit = 'time_limit',
  UserBusy = 'user_busy',
  NotFound = 'not_found'
}
