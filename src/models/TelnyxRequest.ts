export interface TelnyxRequest {
  client_state?: string;
  command_id?: string;
  [key: string]: string | undefined;
}
