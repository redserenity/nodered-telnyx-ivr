import { IStorageApi } from './IStorageApi';

export interface ISettings {
	paletteCategories: string[],
	functionGlobalContext: any,
	storageModule: IStorageApi,
	userDir: string,
	nodesDir: string,
	flowFile: string,
	set: Function,
	verbose: boolean,
	logging?: any;
	uiHost?: string;
	uiPort?: number;
	httpAdminRoot?: string;
	httpAdminAuth?: any;
	httpNodeRoot?: string | boolean;
	httpNodeAuth?: any;
	httpRoot?: string;
	https?: boolean;
	disableEditor?: boolean;
	httpStatic?: string;
	httpStaticAuth?: any;
	httpNodeCors?: any;
	httpNodeMiddleware?: any;
	adminAuth?: any;
	apiMaxLength?: string;
}
