import { MessageCallback } from '../lib/Types';
import { ICredentials } from './ICredentials';
import { IStatusUpdate } from './IStatusUpdate';
import { IMessage } from './IMessage';
import { NodeEvent } from './NodeEvent';

export interface IThisNode {
	id: string;
	type: string;
	credentials?: ICredentials;

	on(event: NodeEvent, callback: MessageCallback): void;
	send(msg: IMessage<any> | Array<IMessage<any> | undefined> | undefined): void;
	receive(msg: IMessage<any> | Array<IMessage<any> | undefined> | undefined): void;
	status(newStatus: IStatusUpdate): void;
	log(msg: string, origMsg?: IMessage<any>): void;
	warn(msg: string, origMsg?: IMessage<any>): void;
	error(msg: string, origMsg?: IMessage<any>): void;

	context(): any;


}
