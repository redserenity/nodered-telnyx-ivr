export enum TelnyxCallState {
  Answered = 'answered',
  Bridged = 'bridged',
  Hangup = 'hangup',
  Parked = 'parked'
}
